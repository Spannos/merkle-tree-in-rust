# Merkle Tree Implementation in Rust

This repository contains a Rust implementation of a Merkle tree, a fundamental data structure used in cryptographic and blockchain technologies. The implementation focuses on the core aspects of a Merkle tree, including the creation of nodes and the computation of hashes using the SHA256 algorithm.

## Features

- **MerkleTree struct:** Represents the Merkle tree with an optional root node.
- **MerkleNode struct:** Represents each node in the Merkle tree.
- **new_leaf and new_parent functions:** Facilitate the creation of leaf and parent nodes.
- **hash function:** Utilizes SHA256 to hash the node data.
- **build_tree_level function:** Constructs one level of the Merkle tree at a time.
- **Display trait implementation for MerkleTree:** Enables printing the root hash of the Merkle tree.

## Installation

Ensure you have Rust and Cargo installed on your machine. If not, you can install them by following the instructions on the [official Rust website](https://www.rust-lang.org/learn/get-started).

### Steps to Install

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/Spannos/merkle-tree-in-rust.git
   cd merkle-tree-in-rust
   ```

2. Build the project:
   ```bash
   cargo build
   ```

## Usage

After installing, you can run the project using Cargo:

```bash
cargo run
```

This command will execute the `main` function in your `main.rs` file, constructing a Merkle tree from the provided data blocks and printing its root hash: `Merkle Tree Root: 48d3e636f2d1997f331f3e7189377f89bab8d81987246f746838f5bbad2ab2ae`

## Limitations

- The current implementation assumes that the number of leaves in the Merkle tree is a power of 2. Handling cases where the number of leaves is not a power of 2 is not yet implemented.
- Functionalities for proof generation and verification are not included in this basic implementation.

## Contributing

Contributions to enhance this project, such as adding support for non-power-of-2 leaf counts or implementing proof generation and verification, are welcome.

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Commit your changes (`git commit -am 'Add some feature'`).
4. Push to the branch (`git push origin feature-branch`).
5. Create a new Pull Request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
```
