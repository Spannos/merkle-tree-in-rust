// Import the Sha256 hashing function and Digest trait from the sha2 crate.
use sha2::{Sha256, Digest};
// Import formatting tools from the standard library.
use std::fmt::{self, Formatter, Display};

// Define a struct for the Merkle Tree, containing an optional root node.
struct MerkleTree {
    root: Option<Box<MerkleNode>>,
}

// Define a struct for nodes in the Merkle Tree.
#[allow(dead_code)] // For future use
#[derive(Clone)]
struct MerkleNode {
    value: String, // The hashed value of the node.
    left: Option<Box<MerkleNode>>, // The left child of the node.
    right: Option<Box<MerkleNode>>, // The right child of the node.
}

// Implement methods for MerkleTree.
impl MerkleTree {
    // Public function to create a new Merkle Tree from a slice of data.
    pub fn new(data: &[String]) -> MerkleTree {
        // Convert the data slice into MerkleNode leaves and collect into a vector.
        let mut nodes = data.iter()
                            .map(|d| MerkleNode::new_leaf(d))
                            .collect::<Vec<_>>();

        // Keep building the tree level by level until only the root node remains.
        while nodes.len() > 1 {
            nodes = MerkleTree::build_tree_level(nodes);
        }

        // Create the MerkleTree with the remaining node as the root.
        MerkleTree {
            root: nodes.pop(),
        }
    }

    // Private function to build one level of the tree.
    fn build_tree_level(mut current_level: Vec<Box<MerkleNode>>) -> Vec<Box<MerkleNode>> {
        let mut next_level = Vec::new(); // Vector to hold the next level of nodes.
        // Process pairs of nodes to create their parent until the current level is empty.
        while !current_level.is_empty() {
            // Take the left node from the current level.
            let left = current_level.remove(0);
            // Take the right node, or duplicate the left if there is no right node.
            let right = if !current_level.is_empty() { current_level.remove(0) } else { left.clone() };
            // Create a parent node from the left and right nodes.
            let parent = MerkleNode::new_parent(&left, &right);
            // Add the parent node to the next level.
            next_level.push(parent);
        }
        // Return the next level of nodes.
        next_level
    }
}

// Implement methods for MerkleNode.
impl MerkleNode {
    // Private function to create a new leaf node.
    fn new_leaf(data: &String) -> Box<MerkleNode> {
        Box::new(MerkleNode {
            value: MerkleNode::hash(data), // Hash the data to create the node value.
            left: None, // Leaf nodes do not have children.
            right: None,
        })
    }

    // Private function to create a new parent node from two child nodes.
    fn new_parent(left: &Box<MerkleNode>, right: &Box<MerkleNode>) -> Box<MerkleNode> {
        Box::new(MerkleNode {
            // Hash the concatenated values of the child nodes to create the parent node value.
            value: MerkleNode::hash(&format!("{}{}", left.value, right.value)),
            // Set the left and right children of the parent node.
            left: Some(left.clone()), // Clone directly
            right: Some(right.clone()), // Clone directly
        })
    }

    // Private function to hash a string using SHA256.
    fn hash(data: &String) -> String {
        let mut hasher = Sha256::new(); // Create a new SHA256 hasher.
        hasher.update(data.as_bytes()); // Add the data to the hasher.
        let result = hasher.finalize(); // Finalize the hashing process.
        format!("{:x}", result) // Return the hash as a hexadecimal string.
    }
}

// Implement the Display trait for MerkleTree to enable formatted printing.
impl Display for MerkleTree {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match &self.root {
            Some(node) => write!(f, "{}", node.value), // Print the value of the root node.
            None => write!(f, "Empty tree"), // Print a message if the tree is empty.
        }
    }
}

// The main function, entry point of the program.
fn main() {
    // Create a vector of data blocks (as Strings).
    let data = vec![
        "Data Block 1".to_string(),
        "Data Block 2".to_string(),
        "Data Block 3".to_string(),
        "Data Block 4".to_string(),
    ];

    // Construct a Merkle tree from the data blocks.
    let merkle_tree = MerkleTree::new(&data);
    // Print the root hash of the Merkle tree.
    println!("Merkle Tree Root: {}", merkle_tree);
}

